#!/usr/bin/env bash


source "$HOME/.exports"
source "$HOME/.aliases"
source "$HOME/.bash_prompt"
source "$HOME/.cargo/env"
source "$HOME/.fzf.bash"

eval $(dircolors -b $HOME/.dircolors)

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/sjoerd/.sdkman"
[[ -s "/Users/sjoerd/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/sjoerd/.sdkman/bin/sdkman-init.sh"
